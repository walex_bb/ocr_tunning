#ifndef COMMON_H
#define COMMON_H

#ifdef ANDROID

    #include <opencv2/opencv.hpp>
	
	#include <api/baseapi.h>
#else

    #if defined(__APPLE__)

        #include "TargetConditionals.h"

        #if defined(TARGET_OS_IPHONE)

            #include "opencv2/core/core.hpp"
            #include "opencv2/imgproc/imgproc.hpp"
            #define IOS
			
			#include <tesseract/baseapi.h>
        #endif
	#else

        #if defined(WIN32) || defined(WIN64)

            #include <opencv2/core.hpp>
            #include <opencv2/imgcodecs.hpp>
            #include <opencv2/imgproc.hpp>
            #include <opencv2/highgui.hpp>
            #include <opencv2/features2d.hpp>

            #ifdef _MSC_VER

                #define pclose _pclose
                #define popen _popen
            #endif

        #endif

    #endif

#endif

#endif
