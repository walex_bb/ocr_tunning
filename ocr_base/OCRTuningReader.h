#pragma once
#include <string>
#include <vector>

#include "common.h"

using namespace std;
using namespace cv;

class OCRTuningReader;

#define HFCR	OCRTuningReader *

// Card Image Processing
HFCR OCRTuning_Initialize(const string & lang, const string & appPath, const std::map<std::string, std::string> * const config);
void OCRTuning_Terminate(HFCR handle);
bool OCRTuning_Process(HFCR handle, const string & oname, Mat& img, string& number, vector<cv::Rect> & bbs);


