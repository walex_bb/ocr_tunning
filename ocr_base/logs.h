#ifndef LOGS_H
#define LOGS_H

#define  LOG_TAG    "OCRTuningReader"
#ifdef __ANDROID__ 

#ifdef NDEBUG2

#define LOGD(...)

#else
	
#include <android/log.h>

#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)

#endif


#else
	
	#if defined(WIN32) && defined(_MSC_VER)

		#include <windows.h>
		#include <stdio.h>
		#define LOGD(...) {\
		char str[512];\
		sprintf_s(str, 512, __VA_ARGS__);\
		OutputDebugStringA(str);}
	#else
		
        #if defined(DEBUG)
            #define LOGD(...) printf(__VA_ARGS__)
        #else
            #define LOGD(...)
        #endif
	#endif

#endif

#endif
