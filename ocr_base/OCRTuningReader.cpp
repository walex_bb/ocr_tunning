#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdexcept>
#include <regex>
#include <math.h>
#include <iostream>
#include <algorithm>

#include "OCRTuningReader.h"
#include "logs.h"

class OCRTuningReader {
    
public:
    
    OCRTuningReader(const string & lang, const string & appPath, const std::map<std::string, std::string> * const config)
	: mConfig(config) {
        
        this->isInitialized = this->initialize(lang, appPath);
    }
    
    ~OCRTuningReader() {
        
        this->terminate();
        this->isInitialized = false;
    }
    
    /**
     public methods definitions
     */
    
    bool process(const string & oname, Mat& img, string& number, vector<cv::Rect> & bbs);
    
private:
    /**
     private varaibles definitions
     */
    
#if defined(ANDROID) || defined(IOS)
    tesseract::TessBaseAPI api;
#endif

    std::string language;
    bool isInitialized;
    
	const std::map<std::string, std::string> * mConfig;
    /**
     private methods definitions
     */
    
    // initialize functions
    bool initialize(const string & lang, const string & appPath);
    void terminate();
    bool TessAPIInit(const std::string & appPath, const std::string & lang);
    void TessAPIEnd();
    
};


#if defined(ANDROID) || defined(IOS)

bool OCRTuningReader::TessAPIInit(const std::string & appPath, const std::string & lang) {
    
    if (api.Init(appPath.c_str(), lang.c_str(), tesseract::OEM_TESSERACT_ONLY)) {
        
        LOGD("Error init tesseract  %s\n", lang.c_str());
        return false;
    }    
	
    LOGD("configuring Tesseract page mode %s\n", lang.c_str());
    api.SetPageSegMode(tesseract::PSM_AUTO);
    
	/*
    //LOGD("configuring Tesseract whitelist %s\n", lang.c_str());
    api.SetVariable("tessedit_char_whitelist", "0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm/%-.");
    
    api.SetVariable("chop_enable","T");
    LOGD("configuring chop enable %s\n", lang.c_str());
	*/
	
	if ( this->mConfig != nullptr) {
		
		for (const auto& kv : *this->mConfig) {
			
			api.SetVariable(kv.first.c_str(), kv.second.c_str());
			LOGD("param setted -> %s %s", kv.first.c_str(), kv.second.c_str());
		}
	}
    return true;
}

void OCRTuningReader::TessAPIEnd() {
    
    api.Clear();
    LOGD("Clear api ok\n");
    
    api.End();
    LOGD("End api ok\n");
}

#endif

bool OCRTuningReader::initialize(const string & lang, const string & appPath) {
    
#if defined(ANDROID) || defined(IOS)
    
    LOGD("App path is %s", appPath.c_str());
    
    if (TessAPIInit(appPath, lang.c_str()) != true) {
        
		LOGD("Tesseract init error");
        return false;
    }
    
#endif
    
    language = lang;
    
	LOGD("Tesseract initialized");
    return true;
}

void OCRTuningReader::terminate() {
    
#if defined(ANDROID) || defined(IOS)
    
    TessAPIEnd();
#endif
}

/**
 Process image card to obtain number.
 */
bool OCRTuningReader::process(const string & oname, Mat& img, string& number, vector<cv::Rect> & bbs)
{ 
    bool result = false;
	
    if (this->isInitialized == false) {
        
        LOGD("flat card reader not initialized\n");
        return result;
    }
    
    LOGD("Processing detection\n");    
	
	Mat grayImg;
    cvtColor(img, grayImg, cv::COLOR_BGR2GRAY);
	
    api.SetImage(static_cast<unsigned char *>(grayImg.data), grayImg.cols, grayImg.rows,
                 grayImg.channels(), grayImg.step);
    
    LOGD("getting Tesseract chars\n");
    number = api.GetUTF8Text();
    LOGD("end getting Tesseract chars %s\n", number.c_str());        
   
	result  = (number.size() > 0);
    return result;
}

/**
 Public API
 */

HFCR OCRTuning_Initialize(const string & lang, const string & appPath, const std::map<std::string, std::string> * const config) {
    
    return reinterpret_cast<HFCR>(new OCRTuningReader(lang, appPath, config));
}

void OCRTuning_Terminate(HFCR handle) {
    
    delete reinterpret_cast<OCRTuningReader*>(handle);
}

bool OCRTuning_Process(HFCR handle, const string & oname, Mat& img, string& number, vector<cv::Rect> & bbs) {
    
    return (reinterpret_cast<OCRTuningReader*>(handle))->process(oname, img, number, bbs);
}