package android.dsense;

/**
 * Created by wadrw on 8/27/2018.
 */

public class OCRTuningConfig {

    static {
        System.loadLibrary("pngt");
        System.loadLibrary("jpgt");
        System.loadLibrary("lept");
        System.loadLibrary("opencv_java3");
        System.loadLibrary("tess");
        System.loadLibrary("ocrtuning");
    }

    public OCRTuningConfig() {

        mHandle = this.create();
    }

    public void set(String key, String value) {

        this.configure(mHandle, key, value);
    }

    public long handle()
    {
        return mHandle;
    }

    @Override
    public void finalize() {

        this.destroy(mHandle);
    }

    private long mHandle;
    private native long create();
    private native void destroy(long handle);
    private native void configure(long handle, String key, String value);
}
