package android.dsense;

import android.graphics.Bitmap;
import android.graphics.Rect;

import java.util.ArrayList;

/**
 * Created by wadrw on 7/25/2018.
 */

public class OCRTuning {

    static {
        System.loadLibrary("pngt");
        System.loadLibrary("jpgt");
        System.loadLibrary("lept");
        System.loadLibrary("opencv_java3");
        System.loadLibrary("tess");
        System.loadLibrary("ocrtuning");
    }
    static final String OCR_TRAIN_ENG = "eng";
    private native String ocrTuningDetect(String appPath, String trainset,  long hConfig, Bitmap image, ArrayList<Rect> lstBB);

    public String detect(String appPath, OCRTuningConfig config, Bitmap image, ArrayList<Rect> lstBB) {

        if (lstBB == null) {
            lstBB = new ArrayList<Rect>();
        } else {

            lstBB.clear();
        }

        long handle = (config != null) ? config.handle() : 0;
        String digits = ocrTuningDetect(appPath, OCR_TRAIN_ENG , handle, image, lstBB);
        return digits;
    }
}
