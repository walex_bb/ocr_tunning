LOCAL_PATH := $(call my-dir)

$(info $(TARGET_ARCH_ABI))

include $(CLEAR_VARS)

LOCAL_MODULE := libocrtuning

TESSERACT_INCLUDE_PATH := $(LOCAL_PATH)/../../../tesseract/tess-two/tess-two/jni/com_googlecode_tesseract_android/src
TESSERACT_LIBS_PATH := $(LOCAL_PATH)/../../../tesseract/tess-two/tess-two/libs
OPENCV_INCLUDE_PATH := $(LOCAL_PATH)/../../../../../opencv3.4.1-android-sdk/sdk/native/jni/include
OPENCV_LIBS_PATH := $(LOCAL_PATH)/../../../../../opencv3.4.1-android-sdk/sdk/native/libs

LOCAL_SRC_FILES := \
  $(LOCAL_PATH)/ocrlib.cpp\
  $(LOCAL_PATH)/../../OCRTuningReader.cpp
  
  
LOCAL_C_INCLUDES := \
  $(LOCAL_PATH)/../../ \
  $(TESSERACT_PATH) \
  $(OPENCV_INCLUDE_PATH) \
  $(TESSERACT_INCLUDE_PATH) \
  $(TESSERACT_INCLUDE_PATH)/ccutil \
  $(TESSERACT_INCLUDE_PATH)/ccstruct \
  $(TESSERACT_INCLUDE_PATH)/ccmain
	
LOCAL_SHARED_LIBRARIES := opencv_lib libjpgt libpngt liblept libtess
  
LOCAL_LDFLAGS +=-ljnigraphics 
 
LOCAL_CFLAGS := \
  --std=c++11 \
  -DUSE_STD_NAMESPACE \
  -DTEST_ANDROID
  -D'VERSION="Android"' \
  -fpermissive \
  -Wno-deprecated \
  -Wno-shift-negative-value \
  -D_GLIBCXX_PERMIT_BACKWARD_HASH   # fix for android-ndk-r8e/sources/cxx-stl/gnu-libstdc++/4.6/include/ext/hash_map:61:30: fatal error: backward_warning.h: No such file or directory

LOCAL_LDLIBS += \
  -llog -landroid

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := opencv_lib
LOCAL_SRC_FILES := $(OPENCV_LIBS_PATH)/$(TARGET_ARCH_ABI)/libopencv_java3.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libtess
LOCAL_SRC_FILES := $(TESSERACT_LIBS_PATH)/$(TARGET_ARCH_ABI)/libtess.so
include $(PREBUILT_SHARED_LIBRARY)

# needed by libtess

include $(CLEAR_VARS)
LOCAL_MODULE := libjpgt
LOCAL_SRC_FILES := $(TESSERACT_LIBS_PATH)/$(TARGET_ARCH_ABI)/libjpgt.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libpngt
LOCAL_SRC_FILES := $(TESSERACT_LIBS_PATH)/$(TARGET_ARCH_ABI)/libpngt.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := liblept
LOCAL_SRC_FILES := $(TESSERACT_LIBS_PATH)/$(TARGET_ARCH_ABI)/liblept.so
include $(PREBUILT_SHARED_LIBRARY)