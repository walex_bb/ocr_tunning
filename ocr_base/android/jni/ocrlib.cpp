#include <jni.h>
#include <android/bitmap.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "OCRTuningReader.h"
#include "logs.h"

template <typename T>
string to_string(T value)
{
    std::ostringstream os ;
    os << value ;
    return os.str() ;
}

void FillBBArrayJNI(JNIEnv* env, const vector<cv::Rect> & bbs, jobject arrayBB) {
	
	jclass alCls = env->FindClass("java/util/ArrayList");
    jclass rcCls = env->FindClass("android/graphics/Rect");

    if (alCls == nullptr || rcCls == nullptr) {
		
		LOGD("No class jni found");
        return;
    }

	jmethodID alAdd = env->GetMethodID( alCls, "add", "(Ljava/lang/Object;)Z");
	jmethodID constructor = env->GetMethodID( rcCls, "<init>","(IIII)V");   

    if (alAdd == nullptr || constructor == nullptr) {
		
		LOGD("No array object jni methods found");
        return;
    }

	for (const cv::Rect & rc : bbs) {	
		
		jobject obj = env->NewObject(rcCls, constructor, (jint)rc.x, (jint)rc.y, (jint)rc.x + rc.width, (jint)rc.y + rc.height);
		env->CallBooleanMethod(arrayBB, alAdd, obj);
	}
    
}

jstring OCRTuningDetect(JNIEnv* env, jobject thiz, jstring appPath, jstring trainSet, const std::map<std::string, std::string> * const config, jobject bitmap, jobject arrayBB) {
	
	string ocrResult = "invalid";
	const char * ocrFolder = env->GetStringUTFChars(appPath, 0);
	const char * trainDataSet = env->GetStringUTFChars(trainSet, 0);
	
	AndroidBitmapInfo  bmInfo;
	if ( AndroidBitmap_getInfo(env, bitmap, &bmInfo) == ANDROID_BITMAP_RESULT_SUCCESS ) {
		
		LOGD("BMP INFO OK");
		LOGD("%s", to_string(bmInfo.width).c_str());
		LOGD("%s", to_string(bmInfo.height).c_str());
		if ( bmInfo.format == ANDROID_BITMAP_FORMAT_RGBA_8888) {
			
			LOGD("BMP FORMAT OK");
			unsigned char * pixels = nullptr;
			if (AndroidBitmap_lockPixels(env, bitmap, (void**) &pixels ) == ANDROID_BITMAP_RESULT_SUCCESS) {
				
				LOGD("BMP LOCK OK");
				
				Size size = Size(bmInfo.width, bmInfo.height);
				cv::Mat frame(size, CV_8UC4, pixels);
				LOGD("FRAME OK");
				cv::Mat out = cv::Mat(size, CV_8UC3);
				LOGD("OUT OK");
				cvtColor(frame, out, cv::COLOR_RGBA2BGR);
				LOGD("CONV OK");
				AndroidBitmap_unlockPixels(env, bitmap);			
				
				bool result = false;
				std::vector<cv::Rect> bbs;
				
				LOGD("CHECKING OCR");
				HFCR handle;
				if ((handle = OCRTuning_Initialize(trainDataSet, ocrFolder, config)) != nullptr) {
					LOGD("OCR INIT");
					result = OCRTuning_Process(handle, "img1", out, ocrResult, bbs);
					LOGD("OCR DETECT");
					OCRTuning_Terminate(handle);
					LOGD("OCR END");
				}
				if (result == false){
					
					ocrResult = "";
				}
					
				FillBBArrayJNI(env, bbs, arrayBB);
				
				LOGD("CALLING cardOCR OK");
									
			}
		}
	}  
	
	env->ReleaseStringUTFChars(appPath, ocrFolder);
	env->ReleaseStringUTFChars(trainSet, trainDataSet);
	
	return env->NewStringUTF(ocrResult.c_str());
}

extern "C" {
	
	
	jlong Java_android_dsense_OCRTuningConfig_create(JNIEnv* env, jobject thiz) {

		return reinterpret_cast<jlong>(new std::map<std::string, std::string>());
	}

	void Java_android_dsense_OCRTuningConfig_configure(JNIEnv* env, jobject thiz, jlong handle, jstring param, jstring value) {

		const char * cParam = env->GetStringUTFChars(param, 0);
		const char * cValue = env->GetStringUTFChars(value, 0);
		
		std::map<std::string, std::string> * params = reinterpret_cast<std::map<std::string, std::string> *>(handle);
		std::map<std::string, std::string>::iterator it = params->find(cParam);
		if ( it != params->end() ) {
		  
			it->second = cValue;
		} else {
		  
			params->insert(std::make_pair(cParam, cValue)); 
		}
		
		env->ReleaseStringUTFChars(param, cParam);
		env->ReleaseStringUTFChars(value, cValue);
	}

	void Java_android_dsense_OCRTuningConfig_destroy(JNIEnv* env, jobject thiz, jlong handle) {

		delete reinterpret_cast<std::map<std::string, std::string> *>(handle);
	}

	
	JNIEXPORT jstring JNICALL Java_android_dsense_OCRTuning_ocrTuningDetect(JNIEnv* env, jobject thiz, jstring appPath, jstring trainSet, jlong hConfig, jobject bitmap, jobject arrayBB) {
		
		std::map<std::string, std::string> * conf = reinterpret_cast<std::map<std::string, std::string> *>(hConfig);
		return OCRTuningDetect(env, thiz, appPath, trainSet, conf, bitmap, arrayBB);
	}

}